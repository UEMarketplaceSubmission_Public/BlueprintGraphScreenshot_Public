## BlueprintGraphScreenshot

### Get this "BlueprintGraphScreenshot" plugin from:
com.epicgames.launcher://ue/marketplace/content/6221f79da85d4ea083b92382daa28e14

### Open your blueprint edtior. Select the nodes in graph. Scale the node to an appropriate size, such as 1:1. Align it to Top-Left corner and click the "GraphScreenshot" button.

![1](README.res/01_Images/1.png)

### Click the link to open the folder.

![2](README.res/01_Images/2.png)

### Will get the screenshot shown below.

![3s](README.res/01_Images/3.png)