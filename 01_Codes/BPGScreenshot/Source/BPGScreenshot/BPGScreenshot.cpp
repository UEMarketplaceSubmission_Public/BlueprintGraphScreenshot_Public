// Copyright 2020 YeHaike. All Rights Reserved.

#include "BPGScreenshot.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BPGScreenshot, "BPGScreenshot" );
